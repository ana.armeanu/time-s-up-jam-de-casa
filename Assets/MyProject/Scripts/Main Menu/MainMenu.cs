﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainMenu : MonoBehaviour
{
    [SerializeField]
    private Button playBtn;

    [SerializeField]
    private Button creditsBtn;

    [SerializeField]
    private Button hideCreditsBtn;

    [SerializeField]
    private GameObject credits;

    private void Awake()
    {
        credits.SetActive(false);
    }

    private void OnEnable()
    {
        const string playScene = "Level 1";

        playBtn.onClick.AddListener(() => SceneManager.LoadScene(playScene));
        creditsBtn.onClick.AddListener(ShowCredits);
        hideCreditsBtn.onClick.AddListener(HideCredits);
    }

    private void OnDisable()
    {
        playBtn.onClick.RemoveAllListeners();
        creditsBtn.onClick.RemoveAllListeners();
        hideCreditsBtn.onClick.RemoveAllListeners();
    }

    private void ShowCredits() {
        credits.SetActive(true);
        playBtn.gameObject.SetActive(false);
        creditsBtn.gameObject.SetActive(false);
    }

    private void HideCredits()
    {
        credits.SetActive(false);
        playBtn.gameObject.SetActive(true);
        creditsBtn.gameObject.SetActive(true);
    }

}
